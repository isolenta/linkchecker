#-------------------------------------------------
#
# Project created by QtCreator 2013-07-23T00:47:38
#
#-------------------------------------------------

QT       += core gui xml network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = linkchecker
TEMPLATE = app

LIBS += -pg -static-libgcc -lntdll -L../linkchecker -lcurl -lxml2 -liconv
QMAKE_CXXFLAGS_DEBUG += -g3 -pg

INCLUDEPATH += iconv

SOURCES += main.cpp\
        mainwindow.cpp \
    lcurl.cpp \
    siteitem.cpp \
    dispatcher.cpp \
    libxml2reader.cpp \
    pageworker.cpp \
    settings.cpp \
    itemworker.cpp \
    QtCUrl.cpp \
    sitemodel.cpp

HEADERS  += mainwindow.h \
    lcurl.h \
    siteitem.h \
    dispatcher.h \
    libxml2reader.h \
    libxml/xpointer.h \
    libxml/xpathInternals.h \
    libxml/xpath.h \
    libxml/xmlwriter.h \
    libxml/xmlversion.h \
    libxml/xmlunicode.h \
    libxml/xmlstring.h \
    libxml/xmlschemastypes.h \
    libxml/xmlschemas.h \
    libxml/xmlsave.h \
    libxml/xmlregexp.h \
    libxml/xmlreader.h \
    libxml/xmlmodule.h \
    libxml/xmlmemory.h \
    libxml/xmlIO.h \
    libxml/xmlexports.h \
    libxml/xmlerror.h \
    libxml/xmlautomata.h \
    libxml/xlink.h \
    libxml/xinclude.h \
    libxml/valid.h \
    libxml/uri.h \
    libxml/tree.h \
    libxml/threads.h \
    libxml/schematron.h \
    libxml/schemasInternals.h \
    libxml/SAX2.h \
    libxml/SAX.h \
    libxml/relaxng.h \
    libxml/pattern.h \
    libxml/parserInternals.h \
    libxml/parser.h \
    libxml/nanohttp.h \
    libxml/nanoftp.h \
    libxml/list.h \
    libxml/HTMLtree.h \
    libxml/HTMLparser.h \
    libxml/hash.h \
    libxml/globals.h \
    libxml/entities.h \
    libxml/encoding.h \
    libxml/DOCBparser.h \
    libxml/dict.h \
    libxml/debugXML.h \
    libxml/chvalid.h \
    libxml/catalog.h \
    libxml/c14n.h \
    iconv/localcharset.h \
    iconv/libcharset.h \
    iconv/iconv.h \
    pageworker.h \
    settings.h \
    itemworker.h \
    QtCUrl.h \
    curl/typecheck-gcc.h \
    curl/stdcheaders.h \
    curl/multi.h \
    curl/mprintf.h \
    curl/easy.h \
    curl/curlver.h \
    curl/curlrules.h \
    curl/curlbuild.h.in \
    curl/curlbuild.h \
    curl/curl.h \
    sitemodel.h

FORMS    += mainwindow.ui \
    settings.ui

OTHER_FILES += \
    details.txt \
    linkchecker.rc \
    TODO \
    curl/curlbuild.h.cmake

RESOURCES += \
    resources.qrc

TRANSLATIONS += russian.ts
RC_FILE = linkchecker.rc
