#include "pageworker.h"
#include <QUrl>
#include <QRegExp>
#include <QFileInfo>
#include <QtXml/QDomDocument>
#include <QThreadPool>
#include <QMutex>
#include <QTextCodec>
#include <QDebug>
#include <QMessageBox>
#include <QSemaphore>
#include <QSet>

#include "libxml2reader.h"
#include "lcurl.h"
#include "dispatcher.h"

PageWorker::PageWorker(QStack<LcURL *> *_curls, QString pageURL)
    : curls(_curls)
{
    url = LcURL::encodeURL(pageURL);
    requestStop = false;
}

QMutex libxmlMutex;

void PageWorker::onStopSignal()
{
    requestStop = true;
}

void PageWorker::followTree(QList<Link> *links, QDomElement node, bool insideLink)
{
    QDomNode n = node.firstChild();
    while(!n.isNull())
    {
        QDomElement e = n.toElement();

        if(!e.isNull())
        {
            if (e.tagName() == "a")
            {
                Link l;

                l.href = e.attribute("href");
                l.link = true;
                l.image = false;

                links->append(l);

                followTree(links, e, true);
                n = n.nextSibling();
                continue;
            }

            if (e.tagName() == "img")
            {
                Link l;

                l.href = e.attribute("src");
                l.link = insideLink;
                l.image = true;
                links->append(l);
            }

            followTree(links, e, insideLink);
        }

        n = n.nextSibling();
    }
}

void PageWorker::run()
{
    curlsMutex.lock();
    if (curls->isEmpty())
    {
        curlsMutex.unlock();
        return;
    }

    curl = curls->pop();
    curlsMutex.unlock();

    QString pageBody = curl->getURL(url);

    curlsMutex.lock();
    curls->push(curl);
    curlsMutex.unlock();

    // immediately exit if STOP requested
    if (requestStop)
    {
        emit complete(url);
        return;
    }

    // protect HTML DOM build
    libxmlMutex.lock();

    // process pageBody
    QXmlInputSource src;

    LibXml2Reader reader;
    QDomDocument doc;

    src.setData(pageBody);
    doc.setContent(&src, &reader);

    libxmlMutex.unlock();

    QList<Link> links;
    links.clear();

    QDomElement rootElem = doc.documentElement();
    followTree(&links, rootElem, false);

    QList<Link>::const_iterator i = links.constBegin();
    while (i != links.constEnd())
    {
        QString link = (*i).href;

        // skip "javascript:" links
        QRegExp javascriptRx("^javascript\\:");
        javascriptRx.setCaseSensitivity(Qt::CaseInsensitive);
        if (javascriptRx.indexIn(link, 0) != -1)
        {
            ++i;
            continue;
        }

        SiteItem item;
        item.isProcessed = false;
        item.page = url;
        item.uri = resolveLink(item.page, link);
        item.isLink = (*i).link;
        item.isImage = (*i).image;

        // immediately exit if STOP requested
        if (requestStop)
        {
            emit complete(url);
            return;
        }

        // request dispatcher to schedule new thread to process this item
        emit scheduleItem(item);
        ++i;
    }

    emit complete(url);
}
