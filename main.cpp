#include "mainwindow.h"
#include <QApplication>
#include <QtCore>
#include <QTranslator>

#include "siteitem.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTranslator translator;
    translator.load("russian");
    a.installTranslator(&translator);

    MainWindow w;

    qRegisterMetaType<SiteItem>("SiteItem");

    w.show();
    
    return a.exec();
}
