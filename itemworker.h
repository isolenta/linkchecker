#ifndef ITEMWORKER_H
#define ITEMWORKER_H

#include <QRunnable>
#include <QObject>
#include <QString>
#include <QSemaphore>
#include <QStack>

#include "siteitem.h"
#include "lcurl.h"

QString resolveLink(const QString &parentPage, const QString &link);

class ItemWorker : public QObject, public QRunnable
{
    Q_OBJECT

public:
    ItemWorker(QStack<LcURL *> *_curls, SiteItem, QString);

    void run();

    QString getUrlFileType(const QString &url);
    QString getUrlDomain(const QString &url);
    bool isLocalDomain(const QString &link);

private slots:
    void onStopSignal();

signals:
    void processPage(const QString &);
    void itemAppend(const SiteItem &);

private:
    SiteItem item;
    QString lDomain;
    bool requestStop;
    QStack<LcURL *> *curls;
    LcURL *curl;
};

#endif // ITEMWORKER_H
