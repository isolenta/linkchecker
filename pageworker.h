#ifndef PAGEWORKER_H
#define PAGEWORKER_H

#include <QRunnable>
#include <QObject>
#include <QString>
#include <QSemaphore>
#include <QtXml/QDomDocument>
#include <QSemaphore>
#include <QStack>
#include <QSet>

#include "lcurl.h"
#include "siteitem.h"

class Link
{
public:
    QString href;
    bool image;
    bool link;

    bool operator==(const Link &other)
    {
        return (href == other.href);
    }

};

inline uint qHash(const Link &key)
{
    return qHash(key.href);
}

QString resolveLink(const QString &parentPage, const QString &link);

class PageWorker : public QObject, public QRunnable
{
    Q_OBJECT

public:
    PageWorker(QStack<LcURL *> *_curls, QString pageURL);

    void followTree(QList<Link> *links, QDomElement node, bool insideLink);
    void run();

private slots:
    void onStopSignal();

signals:
    void scheduleItem(const SiteItem &);
    void complete(const QString &);

private:
    QString url;
    bool requestStop;
    QStack<LcURL *> *curls;
    LcURL *curl;
};

#endif // PAGEWORKER_H
