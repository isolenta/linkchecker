#ifndef SITEMODEL_H
#define SITEMODEL_H

#include <QAbstractTableModel>
#include <QList>
#include <QSet>
#include "siteitem.h"

class SiteModel : public  QAbstractTableModel
{
    Q_OBJECT
public:
    SiteModel(QObject *parent = NULL);
    ~SiteModel();

    void setTypeMap(QHash<QString, QString> *typeMap);
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    int totalRowCount();

    void addSiteItem(const SiteItem &item);
    void addSiteItems(QSet<SiteItem> &items);
    void clear();
    void setFilters(  bool useFilters,
                      bool expOK,
                      bool expBrokenLinks,
                      bool expBrokenImages,
                      bool expBrokenLinkedImages,
                      QString onlyTypes);

    bool isFiltersPassed(const SiteItem &item);
    void applyFilters();

    void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);

    bool useFilters;
    bool expOK;
    bool expBrokenLinks;
    bool expBrokenImages;
    bool expBrokenLinkedImages;
    QString onlyTypes;

private:
    QList<SiteItem> items;
    QList<SiteItem> viewItems;
    QHash<QString, QString> *typeMap;
};

#endif // SITEMODEL_H
