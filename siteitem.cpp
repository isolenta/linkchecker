#include <QSet>
#include "siteitem.h"
/*
SiteItem::SiteItem()
{
    uri = "";
    page = "";
    isOk = false;
    isLocal = false;
    contentType = "";
    fileType = "";
    isImage = false;
    isLink = false;
    isProcessed = false;
    replyCode = "";
}
*/
/*
SiteItem::SiteItem(const SiteItem &item)
{
    uri = item.uri;
    page = item.page;
    isOk = item.isOk;
    isLocal = item.isLocal;
    contentType = item.contentType;
    fileType = item.fileType;
    isImage = item.isImage;
    isLink = item.isLink;
    isProcessed = item.isProcessed;
    replyCode = item.replyCode;
}
*/

QString SiteItem::asString()
{
    QString s = QString("URI=%1 page=%2 ok:%3 local:%4 image:%5 link:%6 proc:%7 fileType=%8 contentType=%9 replyCode=%10").
            arg(uri).
            arg(page).
            arg((int)isOk).
            arg((int)isLocal).
            arg((int)isImage).
            arg((int)isLink).
            arg((int)isProcessed).
            arg(fileType).
            arg(contentType).
            arg(replyCode);
    return s;
}

bool SiteItem::operator==(const SiteItem &item) const
{
    return (item.uri == uri);
}

uint qHash(const SiteItem &key)
{
    return qHash(key.uri) ^ qHash(key.page);
}
