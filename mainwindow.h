#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenu>
#include <QAction>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QLabel>
#include <QTimer>

#include "lcurl.h"
#include "dispatcher.h"
#include "sitemodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void outText(QString text);
    void closeEvent(QCloseEvent *event);

    void exportData(bool expOK,
                    bool expBrokenLinks,
                    bool expBrokenImages,
                    bool expBrokenLinkedImages,
                    QString onlyTypes);

    // settings values
    int viewColumnWidth0;
    int viewColumnWidth1;
    int viewColumnWidth2;
    int viewColumnWidth3;
    int viewColumnWidth4;
    int viewColumnWidth5;
    QString lastStartPage;
    QString userAgent;
    bool useProxy;
    QString proxyURL;
    QString proxyUser;
    QString proxyPass;
    int numThreads;
    QByteArray settingsGeometry;
    QByteArray settingsWindowState;
    bool filtersEnabled;
    bool filtersShowOK;
    bool filtersShowBad;
    QString filtersTypes;
    int networkTimeout;
    QHash<QString, QString> typeMap;
    QString lastSaveDir;

    void loadSetingsFromFile(QString filename);
    void saveSetingsToFile(QString filename);

private slots:
    void siteCompleted();
    void itemsUpdate(const SiteItem &, int, int);
    void printMessage(QString s);

    void exportBrokenLinks();
    void exportBrokenImages();
    void exportBrokenLinkImages();
    void exportOK();
    void exportFiltered();
    void on_btnFilters_clicked(bool checked);
    void on_btnStart_clicked();
    void on_btnStop_clicked();
    void on_btnExport_clicked();
    void on_leURL_textEdited(const QString &arg1);
    void on_btnSettings_clicked();
    void on_tableView_clicked(const QModelIndex &index);
    void settingsUpdated();
    void on_cbShowOK_clicked();
    void on_cbShowBAD_clicked();
    void on_leShowTypes_editingFinished();

    void leURLReturnPressed();
    void requestUpdate();

    private:
    Ui::MainWindow *ui;
    LcURL *lcURL;
    Dispatcher *disp;
    QMenu *exportsMenu;

    QAction *actBrokenLinks;
    QAction *actBrokenImages;
    QAction *actBrokenLinkImages;
    QAction *actOK;
    QAction *actFiltered;    
    SiteModel *viewModel;
    QLabel *lblCounter;    
    bool inProcess;
    QTimer *viewUpdateTimer;
};

#endif // MAINWINDOW_H
