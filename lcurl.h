#ifndef LCURL_H
#define LCURL_H

#include <QString>
#include "QtCUrl.h"

class LcURL
{
public:
    LcURL();
    ~LcURL();

    // check URL health (true is 200 OK response code)
    // fetch Content-Type from headers
    bool checkURL(const QString &url, QString *contentType, QString *replyCode);

    // get contents of requested document
    QString getURL(const QString &url);

    static QString getUserAgent();
    static void setUserAgent(QString ua);

    static bool getUseProxy();
    static void setUseProxy(bool up);

    static QString getProxyURL();
    static void setProxyURL(QString url);

    static QString getProxyUser();
    static void setProxyUser(QString user);

    static QString getProxyPass();
    static void setProxyPass(QString pass);

    static int getTimeout();
    static void setTimeout(int);

    static void setDebugFile(QString filename);

    static QString encodeURL(const QString &url);

private:
    QtCUrl *curl;
    QtCUrl::Options opt;
};


#endif // LCURL_H
