#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <QList>
#include <QThreadPool>
#include <QSemaphore>
#include <QStack>
#include <QMutex>
#include <QSet>
#include <QWaitCondition>
#include "siteitem.h"
#include "itemworker.h"
#include "lcurl.h"

class Dispatcher : public QObject
{
    Q_OBJECT
private:
    // list of processed links: (uri:page)
    QSet<QString> processedLinks;

    // list of URLs of PAGES already precessed
    QSet<QString> itemsCompleted;

    QString localDomain;
    QString startURL;

    bool requestStop;

    int scheduledItems, scheduledPages;
    QThreadPool *itemsPool;
    QThreadPool *pagesPool;

    int itemsPoolSize;
    int pagesPoolSize;
    int globalPoolSize;
    int totalCount;
    int errCount;

    QStack <LcURL *> curls;
    QSet<SiteItem> lastPiece;
signals:
    // emitted when items list updated by workers to update UI
    void itemsListUpdated(const SiteItem &, int, int);

    // emitted when site completed
    void completed();

    // send STOP to workers
    void workersStop();

public slots:
    void schedulePageProcessing(const QString &link);
    void scheduleItemProcessing(const SiteItem &item);

    void pageCompleted(const QString &);
    void itemAppend(const SiteItem &);

public:
    Dispatcher(int poolSize, int networkTimeout);
    ~Dispatcher();

    void start(QString startURL);
    void sendStop();
    void showItems();
    QThreadPool *getItemsPool() { return itemsPool; }
    QThreadPool *getPagesPool() { return pagesPool; }
    void requestLastPiece(QSet<SiteItem> *piece);
};

extern QMutex curlsMutex;

class Sleep
{

public:

    // Causes the current thread to sleep for msecs milliseconds.
    static void msleep(unsigned long msecs)
    {
        QMutex mutex;
        mutex.lock();

        QWaitCondition waitCondition;
        waitCondition.wait(&mutex, msecs);

        mutex.unlock();
    }
};

#endif // DISPATCHER_H
