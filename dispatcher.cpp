#include <QRegExp>
#include <QMessageBox>
#include <QDebug>
#include <QtXml/QDomDocument>
#include <QUrl>
#include <QFileInfo>
#include <QThreadPool>
#include <QMutex>

#include "dispatcher.h"
#include "siteitem.h"
#include "lcurl.h"
#include "libxml2reader.h"
#include "pageworker.h"
#include "itemworker.h"
#include "lcurl.h"

QMutex curlsMutex;

QString resolveLink(const QString &parentPage, const QString &link)
{
    QUrl baseURL(parentPage);
    QUrl relativeURL(link);

    return baseURL.resolved(relativeURL.toString()).toString();
}

Dispatcher::Dispatcher(int poolSize, int networkTimeout)
{
    requestStop = false;
    scheduledItems = scheduledPages = 0;
    totalCount = 0;
    errCount = 0;
    processedLinks.clear();

    itemsPool = new QThreadPool(this);
    pagesPool = new QThreadPool(this);

    globalPoolSize = poolSize;

    itemsPoolSize = globalPoolSize / 2;
    pagesPoolSize = globalPoolSize - itemsPoolSize;

    itemsPool->setMaxThreadCount(itemsPoolSize);
    pagesPool->setMaxThreadCount(pagesPoolSize);

    itemsPool->setExpiryTimeout(networkTimeout);
    pagesPool->setExpiryTimeout(networkTimeout);

    // initialize curls pool
    for (int i = 0; i < globalPoolSize+1; i++)
    {
        LcURL *c = new LcURL;
        curls.push(c);
    }
}

Dispatcher::~Dispatcher()
{
    delete itemsPool;
    delete pagesPool;
}

void Dispatcher::pageCompleted(const QString &page)
{
    scheduledPages--;

    itemsCompleted.insert(page);

    if (        (itemsPool->activeThreadCount() == 0) &&
                (pagesPool->activeThreadCount() == 0))
    {
        emit completed();
    }
}

// append new item to collection
void Dispatcher::itemAppend(const SiteItem &item)
{
    if (processedLinks.contains(item.uri+item.page))
        goto out;

    scheduledItems--;

    totalCount++;

    processedLinks.insert( item.uri+item.page );

    // auto-schedulting start page
    if (item.uri == this->startURL)
        schedulePageProcessing(item.uri);

    if (!item.isOk)
        errCount++;

    // notify UI that need to append new item
    emit itemsListUpdated(item, totalCount, scheduledItems + scheduledPages);
    lastPiece.insert(item);

out:
    if (        (itemsPool->activeThreadCount() == 0) &&
                (pagesPool->activeThreadCount() == 0))
    {
        emit completed();
    }
}

// append item to process
void Dispatcher::scheduleItemProcessing(const SiteItem &item)
{
    // do not schedule new worker if STOP requested
    if (requestStop)
        return;

    // do not duplicate scheduling pair URL:PAGE
    if (processedLinks.contains(item.uri+item.page))
    {
        scheduledItems--;
        return;
    }

    ItemWorker *w = new ItemWorker(&curls, item, localDomain);
    w->setAutoDelete(true);

    connect(w, SIGNAL(processPage(const QString &)), this, SLOT(schedulePageProcessing(const QString &)));
    connect(w, SIGNAL(itemAppend(const SiteItem &)), this, SLOT(itemAppend(const SiteItem &)));
    connect(this, SIGNAL(workersStop()), w, SLOT(onStopSignal()));

    // start new task to run
    scheduledItems++;
    itemsPool->start(w);
}

// append page to process
void Dispatcher::schedulePageProcessing(const QString &link)
{
    // do not schedule new worker if STOP requested
    if (requestStop)
        return;

    // if already processed, nothing to do
    if (itemsCompleted.contains(link))
    {
        scheduledPages--;
        return;
    }

    PageWorker *w = new PageWorker(&curls, link);
    w->setAutoDelete(true);

    connect(w, SIGNAL(scheduleItem(const SiteItem &)), this, SLOT(scheduleItemProcessing(const SiteItem &)));
    connect(w, SIGNAL(complete(const QString &)), this, SLOT(pageCompleted(const QString &)));

    connect(this, SIGNAL(workersStop()), w, SLOT(onStopSignal()));

    itemsCompleted.insert(link);

    // start new task to run
    scheduledPages++;
    pagesPool->start(w);
}

void Dispatcher::start(QString startURL)
{
    QUrl url(startURL);
    localDomain = url.host();

    // clear item lists
    itemsCompleted.clear();
    processedLinks.clear();
    totalCount = 0;
    errCount = 0;

    scheduledItems = scheduledPages = 0;

    requestStop = false;

    this->startURL = startURL;

    // schedule checking start item
    SiteItem startItem;

    startItem.page = "";
    startItem.uri = startURL;

    scheduleItemProcessing(startItem);
}

void Dispatcher::showItems()
{
}

void Dispatcher::sendStop()
{
    requestStop = true;
    //emit workersStop();
}

void Dispatcher::requestLastPiece(QSet<SiteItem> *piece)
{
    QSet<SiteItem>::const_iterator i = lastPiece.constBegin();

    while (i != lastPiece.constEnd())
    {
        piece->insert(*i);
        ++i;
    }

    lastPiece.clear();
}
