<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Link checker</source>
        <translation>Link checker</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="32"/>
        <source>Enable filters</source>
        <translation>Включить фильтры</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="67"/>
        <source>http://isolenta.com/page1.html</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <source>Start</source>
        <translation>Запуск</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>Stop</source>
        <translation>Останов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="107"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="137"/>
        <source>Export</source>
        <translation>Экспортировать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="188"/>
        <source>show OK</source>
        <translation>Только рабочие ссылки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <source>show BAD</source>
        <translation>Только битые ссылки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="202"/>
        <source>show types:</source>
        <translation>Показывать типы файлов:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>Ready</source>
        <translation>Готов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="79"/>
        <source>Export broken links</source>
        <translation>Экспортировать битые ссылки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="80"/>
        <source>Export broken images</source>
        <translation>Экспортировать битые изображения</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="81"/>
        <source>Export broken linked images</source>
        <translation>Экспортировать битые изображения-ссылки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="82"/>
        <source>Export correct items</source>
        <translation>Экспортировать рабочие элементы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="83"/>
        <source>Export filtered items</source>
        <translation>Экспортировать отфильтрованные элементы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="274"/>
        <location filename="mainwindow.cpp" line="498"/>
        <location filename="mainwindow.cpp" line="515"/>
        <location filename="mainwindow.cpp" line="532"/>
        <source>%1 total, %2 filtered</source>
        <translation>Всего %1, с учетом фильтра %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="306"/>
        <source>Processed %1 items, queued %2 items</source>
        <oldsource>Processed %1 items</oldsource>
        <translation>Обработано: %1, в очереди: %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="427"/>
        <source>Export links list</source>
        <translation>Экспортировать список ссылок</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <source>Error opening file for writing (%1)</source>
        <translation>Ошибка при сохранении файла (%1)</translation>
    </message>
    <message>
        <source>Link</source>
        <translation type="obsolete">Ссылка</translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="obsolete">Страница</translation>
    </message>
    <message>
        <source>Filetype</source>
        <translation type="obsolete">Тип файлов</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="obsolete">Статус</translation>
    </message>
    <message>
        <source>Content-type</source>
        <translation type="obsolete">Тип содержимого</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="settings.ui" line="26"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="settings.ui" line="36"/>
        <source>use proxy</source>
        <translation>Использовать прокси-сервер</translation>
    </message>
    <message>
        <location filename="settings.ui" line="78"/>
        <source>Proxy host</source>
        <translation>Адрес сервера [:порт]</translation>
    </message>
    <message>
        <location filename="settings.ui" line="120"/>
        <source>Proxy username</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="settings.ui" line="162"/>
        <source>Proxy password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="settings.ui" line="192"/>
        <source>User-Agent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="219"/>
        <source>Num threads *</source>
        <oldsource>Num threads</oldsource>
        <translation>Количество потоков *</translation>
    </message>
    <message>
        <location filename="settings.ui" line="246"/>
        <source>Network timeout (ms)</source>
        <oldsource>Network timeout (ms) *</oldsource>
        <translation>Сетевой таймаут (мс)</translation>
    </message>
    <message>
        <location filename="settings.ui" line="258"/>
        <source>*  restart application to apply</source>
        <translation>* требуется перезапуск приложения</translation>
    </message>
    <message>
        <location filename="settings.ui" line="281"/>
        <source>File types mapping:</source>
        <translation>Ассоциации типов файлов:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="37"/>
        <source>Filetype</source>
        <translation>Тип файла</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="38"/>
        <source>Content-type</source>
        <translation>Тип содержимого</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="63"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="64"/>
        <location filename="settings.cpp" line="77"/>
        <source>Add new</source>
        <translation>Добавить новую ассоциацию</translation>
    </message>
</context>
<context>
    <name>SiteModel</name>
    <message>
        <location filename="sitemodel.cpp" line="142"/>
        <source>Link</source>
        <translation>Ссылка</translation>
    </message>
    <message>
        <location filename="sitemodel.cpp" line="144"/>
        <source>Page</source>
        <translation>Страница</translation>
    </message>
    <message>
        <location filename="sitemodel.cpp" line="146"/>
        <source>Filetype</source>
        <translation>Тип файла</translation>
    </message>
    <message>
        <location filename="sitemodel.cpp" line="148"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="sitemodel.cpp" line="150"/>
        <source>Content-type</source>
        <translation>Тип содержимого</translation>
    </message>
</context>
</TS>
