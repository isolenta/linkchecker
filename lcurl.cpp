// ----------------------------------------------------------------------------
#include <QString>
#include <QRegExp>
#include <QMessageBox>

#include <QFile>
#include <QDebug>
#include <QTextStream>

#include <QUrl>

#include "lcurl.h"
#include "QtCUrl.h"

// ----------------------------------------------------------------------------
static QString userAgent;
static bool useProxy;
static QString proxyURL;
static QString proxyUser;
static QString proxyPass;
static int timeout;
static QString debugFilename;
// ----------------------------------------------------------------------------
LcURL::LcURL()
{
    curl = new QtCUrl;

    opt[CURLOPT_FOLLOWLOCATION] = true;
    opt[CURLOPT_SSL_VERIFYHOST] = false;
    opt[CURLOPT_SSL_VERIFYPEER] = false;
    opt[CURLOPT_HEADER] = true;
    opt[CURLOPT_NOBODY] = true;
    opt[CURLOPT_USERAGENT] = userAgent;
    opt[CURLOPT_TIMEOUT_MS] = timeout;

    if (useProxy)
    {
        opt[CURLOPT_PROXY] = proxyURL;
        opt[CURLOPT_PROXYAUTH] = (int)CURLAUTH_BASIC;
        opt[CURLOPT_PROXYUSERPWD] = QString("%1:%2")
                .arg(proxyUser)
                .arg(proxyPass);
    }
}

LcURL::~LcURL()
{
    delete curl;
}
// ----------------------------------------------------------------------------
QString LcURL::encodeURL(const QString &url)
{
    //return url.toLocal8Bit().toPercentEncoding(":&=/");
    return url;
}
// ----------------------------------------------------------------------------
// perform HEAD request to URL to check HTTP response code
bool LcURL::checkURL(const QString &url, QString *contentType, QString *replyCode)
{
    curl->setTextCodec("UTF-8");

    opt[CURLOPT_URL] = QUrl(url);
    opt[CURLOPT_HEADER] = true;
    opt[CURLOPT_NOBODY] = true;

    QString reply = curl->exec(opt);

    if (debugFilename != "")
    {
        QFile file(debugFilename);
        file.open(QIODevice::Append | QIODevice::Text);
        QTextStream out(&file);
        out << QString("---\n\ncheckURL(%1):\n---\n").arg(url);
        out << reply;
        file.close();
    }

    if (curl->lastError().isOk())
    {
        QRegExp httpCodeRx("HTTP/1\\.(\\d) 200 OK");
        httpCodeRx.setCaseSensitivity(Qt::CaseInsensitive);

        QRegExp contentTypeRx("Content-Type\\: ([a-zA-Z0-9_\\-\\/]+)");

        if (contentTypeRx.indexIn(reply, 0) != -1)
            *contentType = contentTypeRx.cap(1);
        else
            *contentType = "";

        bool replyOk = false;

        if (httpCodeRx.indexIn(reply, 0) != -1)
            replyOk = true;

        QRegExp replyCodeRx("HTTP/1\\.(\\d) (\\d+) ([a-zA-Z0-9_\\-\\/ ]+)");
        if (replyCodeRx.indexIn(reply, 0) != -1)
            *replyCode = QString("%1 %2")
                .arg(replyCodeRx.cap(2))
                .arg(replyCodeRx.cap(3));
        else
            *replyCode = "";

        return replyOk;
    }
    else
    {
        *contentType = "";
        *replyCode = curl->errorBuffer();

        return false;
    }
}
// ----------------------------------------------------------------------------
QString LcURL::getURL(const QString &url)
{
    curl->setTextCodec("UTF-8");

    opt[CURLOPT_URL] = QUrl(url);

    opt[CURLOPT_HEADER] = false;
    opt[CURLOPT_NOBODY] = false;

    QString reply = curl->exec(opt);

    if (debugFilename != "")
    {
        QFile file(debugFilename);
        file.open(QIODevice::Append | QIODevice::Text);
        QTextStream out(&file);
        out << QString("---\n\ngetURL(%1):\n---\n").arg(url);
        out << reply;
        file.close();
    }

    return reply;
}
// ----------------------------------------------------------------------------
QString LcURL::getUserAgent()
{
    return userAgent;
}
// ----------------------------------------------------------------------------
void LcURL::setUserAgent(QString ua)
{
    userAgent = ua;
}
// ----------------------------------------------------------------------------
bool LcURL::getUseProxy()
{
    return useProxy;
}
// ----------------------------------------------------------------------------
void LcURL::setUseProxy(bool up)
{
    useProxy = up;
}
// ----------------------------------------------------------------------------
QString LcURL::getProxyURL()
{
    return proxyURL;
}
// ----------------------------------------------------------------------------
void LcURL::setProxyURL(QString url)
{
    proxyURL = url;
}
// ----------------------------------------------------------------------------
QString LcURL::getProxyUser()
{
    return proxyUser;
}
// ----------------------------------------------------------------------------
void LcURL::setProxyUser(QString user)
{
    proxyUser = user;
}
// ----------------------------------------------------------------------------
QString LcURL::getProxyPass()
{
    return proxyPass;
}
// ----------------------------------------------------------------------------
void LcURL::setProxyPass(QString pass)
{
    proxyPass = pass;
}
// ----------------------------------------------------------------------------
int LcURL::getTimeout()
{
    return timeout;
}
// ----------------------------------------------------------------------------
void LcURL::setTimeout(int t)
{
    timeout = t;
}
// ----------------------------------------------------------------------------
void LcURL::setDebugFile(QString filename)
{
    debugFilename = filename;
}
// ----------------------------------------------------------------------------
// EOF
