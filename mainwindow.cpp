// ----------------------------------------------------------------------------
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "dispatcher.h"
#include <QThreadPool>
#include <QCloseEvent>
#include <QAction>
#include <QStandardItemModel>
#include <QStandardItem>
#include "siteitem.h"
#include "settings.h"
#include <QDebug>
#include <QUrl>
#include <QClipboard>
#include <QSettings>
#include <QFileDialog>
#include <QFileInfo>
#include <QDateTime>
#include <QUrl>
#include <QTextCodec>
#include "sitemodel.h"
// ----------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

    // statusbar entry
    lblCounter = new QLabel;
    lblCounter->setText(tr("Ready"));
    lblCounter->setFrameShadow(QFrame::Plain);
    lblCounter->setFrameShape(QFrame::NoFrame);
    ui->statusBar->addWidget(lblCounter, 0);
    ui->statusBar->setSizeGripEnabled ( false );

    // enable curl debugging
    //LcURL::setDebugFile(QCoreApplication::applicationDirPath() + "/linkchecker.debug.txt");

    // load initial settings
    loadSetingsFromFile(QCoreApplication::applicationDirPath() + "/config.ini");

    restoreGeometry(settingsGeometry);
    restoreState(settingsWindowState);
    ui->btnFilters->setChecked(filtersEnabled);
    ui->frameFilters->setVisible(filtersEnabled);
    ui->cbShowOK->setChecked(filtersShowOK);
    ui->cbShowBAD->setChecked(filtersShowBad);
    ui->leShowTypes->setText(filtersTypes);

    ui->leURL->setText(lastStartPage);
    if (lastStartPage == "")
        ui->btnStart->setEnabled(false);

    LcURL::setTimeout(networkTimeout);
    LcURL::setUserAgent(userAgent);
    LcURL::setUseProxy(useProxy);
    LcURL::setProxyURL(proxyURL);
    LcURL::setProxyUser(proxyUser);
    LcURL::setProxyPass(proxyPass);

    // create dispatcher instance
    disp = new Dispatcher(numThreads, networkTimeout);

    connect(disp, SIGNAL(completed()), this, SLOT(siteCompleted()));
    connect(disp, SIGNAL(itemsListUpdated(const SiteItem &, int, int)), this, SLOT(itemsUpdate(SiteItem, int,int)));

    ui->btnStop->setVisible(false);

    viewUpdateTimer = new QTimer(this);
    connect(viewUpdateTimer, SIGNAL(timeout()), this, SLOT(requestUpdate()));

    exportsMenu = new QMenu(this);

    actBrokenLinks = new QAction(tr("Export broken links"), this);
    actBrokenImages = new QAction(tr("Export broken images"), this);
    actBrokenLinkImages = new QAction(tr("Export broken linked images"), this);
    actOK = new QAction(tr("Export correct items"), this);
    actFiltered = new QAction(tr("Export filtered items"), this);

    exportsMenu->addAction(actBrokenLinks);
    exportsMenu->addAction(actBrokenImages);
    exportsMenu->addAction(actBrokenLinkImages);
    exportsMenu->addAction(actOK);
    exportsMenu->addAction(actFiltered);

    connect(actBrokenLinks, SIGNAL(triggered()), this, SLOT(exportBrokenLinks()));
    connect(actBrokenImages, SIGNAL(triggered()), this, SLOT(exportBrokenImages()));
    connect(actBrokenLinkImages, SIGNAL(triggered()), this, SLOT(exportBrokenLinkImages()));
    connect(actOK, SIGNAL(triggered()), this, SLOT(exportOK()));
    connect(actFiltered, SIGNAL(triggered()), this, SLOT(exportFiltered()));

    // create data model
    viewModel = new SiteModel(this);
    viewModel->setTypeMap(&typeMap);
    viewModel->setFilters(
                ui->btnFilters->isChecked(),
                ui->cbShowOK->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->leShowTypes->text()
    );

    // set custom model to view
    ui->tableView->setModel(viewModel);

    // setting up tableview ui
    ui->tableView->setAlternatingRowColors(true);
    ui->tableView->verticalHeader()->setDefaultSectionSize(20);
    ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView->setSortingEnabled(true);

    ui->tableView->horizontalHeader()->setStretchLastSection(false);
    ui->tableView->horizontalHeader()->setResizeMode(1, QHeaderView::Interactive);

    ui->tableView->setColumnWidth(0, viewColumnWidth0);
    ui->tableView->setColumnWidth(1, viewColumnWidth1);
    ui->tableView->setColumnWidth(2, viewColumnWidth2);
    ui->tableView->setColumnWidth(3, viewColumnWidth3);
    ui->tableView->setColumnWidth(4, viewColumnWidth4);
    ui->tableView->setColumnWidth(5, viewColumnWidth5);

    connect(ui->leURL, SIGNAL(returnPressed()), this, SLOT(leURLReturnPressed()), Qt::QueuedConnection);
}
// ----------------------------------------------------------------------------
MainWindow::~MainWindow()
{
    settingsUpdated();

    delete disp;
    delete ui;
}
// ----------------------------------------------------------------------------
void MainWindow::settingsUpdated()
{
    // save UI state
    viewColumnWidth0 = ui->tableView->columnWidth(0);
    viewColumnWidth1 = ui->tableView->columnWidth(1);
    viewColumnWidth2 = ui->tableView->columnWidth(2);
    viewColumnWidth3 = ui->tableView->columnWidth(3);
    viewColumnWidth4 = ui->tableView->columnWidth(4);
    viewColumnWidth5 = ui->tableView->columnWidth(5);

    settingsGeometry = saveGeometry();
    settingsWindowState = saveState();
    lastStartPage = ui->leURL->text();
    filtersEnabled = ui->btnFilters->isChecked();
    filtersShowOK = ui->cbShowOK->isChecked();
    filtersShowBad = ui->cbShowBAD->isChecked();
    filtersTypes = ui->leShowTypes->text();

    // change runtime parameters
    LcURL::setTimeout(networkTimeout);
    LcURL::setUserAgent(userAgent);
    LcURL::setUseProxy(useProxy);
    LcURL::setProxyURL(proxyURL);
    LcURL::setProxyUser(proxyUser);
    LcURL::setProxyPass(proxyPass);

    // save to config.ini
    saveSetingsToFile(QCoreApplication::applicationDirPath() + "/config.ini");
}
// ----------------------------------------------------------------------------
void MainWindow::loadSetingsFromFile(QString filename)
{
    QSettings settings(filename, QSettings::IniFormat);

    viewColumnWidth0 = settings.value("viewColumnWidth0", 32).toInt();
    viewColumnWidth1 = settings.value("viewColumnWidth1", 260).toInt();
    viewColumnWidth2 = settings.value("viewColumnWidth2", 200).toInt();
    viewColumnWidth3 = settings.value("viewColumnWidth3", 100).toInt();
    viewColumnWidth4 = settings.value("viewColumnWidth4", 50).toInt();
    viewColumnWidth5 = settings.value("viewColumnWidth5", 100).toInt();
    lastStartPage = settings.value("lastStartPage").toString();
    userAgent = settings.value("userAgent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1541.0 Safari/537.36").toString();
    useProxy = settings.value("useProxy").toBool();
    proxyURL = settings.value("proxyURL").toString();
    proxyUser = settings.value("proxyUser").toString();
    proxyPass = settings.value("proxyPass").toString();
    numThreads = settings.value("numThreads", 25).toInt();
    settingsGeometry = settings.value("geometry").toByteArray();
    settingsWindowState = settings.value("windowstate").toByteArray();
    filtersEnabled = settings.value("filtersEnabled").toBool();
    filtersShowOK = settings.value("filtersShowOK").toBool();
    filtersShowBad = settings.value("filtersShowBad").toBool();
    filtersTypes = settings.value("filtersTypes").toString();
    networkTimeout = settings.value("networkTimeout", 5000).toInt();
    lastSaveDir = settings.value("lastSaveDir", QCoreApplication::applicationDirPath()).toString();

    QByteArray serializedTypeMap;
    QDataStream ds(&serializedTypeMap, QIODevice::ReadOnly);
    serializedTypeMap = settings.value("typeMap").toByteArray();
    ds >> typeMap;
}
// ----------------------------------------------------------------------------
void MainWindow::saveSetingsToFile(QString filename)
{
    QSettings settings(filename, QSettings::IniFormat);

    settings.setValue("viewColumnWidth0", viewColumnWidth0 );
    settings.setValue("viewColumnWidth1", viewColumnWidth1 );
    settings.setValue("viewColumnWidth2", viewColumnWidth2 );
    settings.setValue("viewColumnWidth3", viewColumnWidth3 );
    settings.setValue("viewColumnWidth4", viewColumnWidth4 );
    settings.setValue("viewColumnWidth5", viewColumnWidth5 );
    settings.setValue("lastStartPage", lastStartPage );
    settings.setValue("userAgent", userAgent );
    settings.setValue("useProxy", useProxy );
    settings.setValue("proxyURL", proxyURL );
    settings.setValue("proxyUser", proxyUser );
    settings.setValue("proxyPass", proxyPass );
    settings.setValue("numThreads", numThreads );
    settings.setValue("geometry", settingsGeometry);
    settings.setValue("windowstate", settingsWindowState);
    settings.setValue("filtersEnabled", filtersEnabled);
    settings.setValue("filtersShowOK", filtersShowOK);
    settings.setValue("filtersShowBad", filtersShowBad);
    settings.setValue("filtersTypes", filtersTypes);
    settings.setValue("networkTimeout", networkTimeout);
    settings.setValue("lastSaveDir", lastSaveDir);

    QByteArray serializedTypeMap;
    QDataStream ds(&serializedTypeMap, QIODevice::WriteOnly);
    ds << typeMap;

    settings.setValue("typeMap", serializedTypeMap);
}
// ----------------------------------------------------------------------------
void MainWindow::outText(QString text)
{
    Q_UNUSED(text);

    QApplication::processEvents();
}
// ----------------------------------------------------------------------------
void MainWindow::siteCompleted()
{
    ui->btnStop->setVisible(false);
    ui->btnStart->setVisible(true);
    ui->btnStop->setEnabled(true);

    ui->leURL->setEnabled(true);
    ui->btnStart->setEnabled(true);
    ui->btnFilters->setEnabled(true);
    ui->btnSettings->setEnabled(true);
    ui->btnExport->setEnabled(true);
    ui->cbShowOK->setEnabled(true);
    ui->cbShowBAD->setEnabled(true);
    ui->leShowTypes->setEnabled(true);

    viewUpdateTimer->stop();
    requestUpdate();

    ui->tableView->setColumnWidth(0, viewColumnWidth0);
    ui->tableView->setColumnWidth(1, viewColumnWidth1);
    ui->tableView->setColumnWidth(2, viewColumnWidth2);
    ui->tableView->setColumnWidth(3, viewColumnWidth3);
    ui->tableView->setColumnWidth(4, viewColumnWidth4);
    ui->tableView->setColumnWidth(5, viewColumnWidth5);

    lblCounter->setText(QString(tr("%1 total, %2 filtered"))
                            .arg(viewModel->totalRowCount())
                            .arg(viewModel->rowCount(QModelIndex()))
                        );
}
// ----------------------------------------------------------------------------
void MainWindow::printMessage(QString s)
{
    qDebug() << s;
}
// ----------------------------------------------------------------------------
void MainWindow::closeEvent(QCloseEvent *event)
{
    // force stop processing
    ui->btnStop->setEnabled(false);
    disp->sendStop();

    // wait for workers finished
    bool done = false;
    while (!done)
    {
        done = disp->getItemsPool()->waitForDone(50) && disp->getPagesPool()->waitForDone(50);
        QApplication::processEvents();
    }

    event->accept();
}
// ----------------------------------------------------------------------------
void MainWindow::itemsUpdate(const SiteItem &item, int total, int queued)
{
    Q_UNUSED(item);

    lblCounter->setText(QString(tr("Processed %1 items, queued %2 items")).arg(total).arg(queued));
}
// ----------------------------------------------------------------------------
void MainWindow::requestUpdate()
{
    QSet<SiteItem> piece;

    ui->tableView->setUpdatesEnabled(false);
    disp->requestLastPiece(&piece);
    viewModel->addSiteItems(piece);
    ui->tableView->setUpdatesEnabled(true);

    ui->tableView->scrollToBottom();
}
// ----------------------------------------------------------------------------
void MainWindow::on_btnFilters_clicked(bool checked)
{
    ui->frameFilters->setVisible(checked);

    viewModel->setFilters(
                ui->btnFilters->isChecked(),
                ui->cbShowOK->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->leShowTypes->text()
    );
}
// ----------------------------------------------------------------------------
void MainWindow::leURLReturnPressed()
{
    on_btnStart_clicked();
}
// ----------------------------------------------------------------------------
void MainWindow::on_btnStart_clicked()
{
    if (ui->leURL->text() == "")
        return;

    QUrl url(ui->leURL->text());

    if (url.scheme() == "")
        ui->leURL->setText("http://" + ui->leURL->text());

    viewModel->clear();
    disp->start(ui->leURL->text());
    viewUpdateTimer->start(1000);

    ui->leURL->setEnabled(false);
    ui->btnStart->setEnabled(false);
    ui->btnFilters->setEnabled(false);
    ui->btnSettings->setEnabled(false);
    ui->btnExport->setEnabled(false);
    ui->cbShowOK->setEnabled(false);
    ui->cbShowBAD->setEnabled(false);
    ui->leShowTypes->setEnabled(false);

    ui->btnStop->setVisible(true);
    ui->btnStart->setVisible(false);
}
// ----------------------------------------------------------------------------
void MainWindow::on_btnStop_clicked()
{
    ui->btnStop->setEnabled(false);
    disp->sendStop();    
}
// ----------------------------------------------------------------------------
void MainWindow::on_btnExport_clicked()
{
    exportsMenu->exec(QCursor::pos());
}
// ----------------------------------------------------------------------------
void MainWindow::exportBrokenLinks()
{
    exportData(false, true, false, false, "");
}
// ----------------------------------------------------------------------------
void MainWindow::exportBrokenImages()
{
    exportData(false, false, true, false, "");
}
// ----------------------------------------------------------------------------
void MainWindow::exportBrokenLinkImages()
{
    exportData(false, false, false, true, "");
}
// ----------------------------------------------------------------------------
void MainWindow::exportOK()
{
    exportData(true, false, false, false, "");
}
// ----------------------------------------------------------------------------
void MainWindow::exportFiltered()
{
    exportData(ui->cbShowOK->isChecked(),
               ui->cbShowBAD->isChecked(),
               ui->cbShowBAD->isChecked(),
               ui->cbShowBAD->isChecked(),
               ui->leShowTypes->text());
}
// ----------------------------------------------------------------------------
void MainWindow::on_leURL_textEdited(const QString &arg1)
{
    ui->btnStart->setEnabled((bool)(arg1 != ""));
}
// ----------------------------------------------------------------------------
void MainWindow::on_btnSettings_clicked()
{
    Settings settings(this);
    connect(&settings, SIGNAL(settingsUpdated()), this, SLOT(settingsUpdated()));
    settings.exec();
}
// ----------------------------------------------------------------------------
void MainWindow::on_tableView_clicked(const QModelIndex &index)
{
    QString data;

    data = viewModel->data(viewModel->index(index.row(), index.column()), Qt::DisplayRole).toString();

    QApplication::clipboard()->setText(data);
}
// ----------------------------------------------------------------------------
void MainWindow::exportData(bool expOK,
                            bool expBrokenLinks,
                            bool expBrokenImages,
                            bool expBrokenLinkedImages,
                            QString onlyTypes)
{
    QUrl url(ui->leURL->text());
    QDateTime dt = QDateTime::currentDateTime();
    QString dateStr = dt.toString("dd_MM_yyyy_hh-mm");
    QString defaultName = QString("%1_%2.csv")
            .arg(url.host())
            .arg(dateStr);

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Export links list"),
                                                    lastSaveDir + "/" + defaultName,
                                                    "CSV file (*.csv)");
    if (fileName == "")
        return;

    QFileInfo fi(fileName);
    lastSaveDir = fi.absoluteDir().absolutePath();

    // save current filters
    bool savedExpOK = viewModel->expOK;
    bool savedExpBrokenLinks = viewModel->expBrokenLinks;
    bool savedExpBrokenImages = viewModel->expBrokenImages;
    bool savedExpBrokenLinkedImages = viewModel->expBrokenLinkedImages;
    QString savedOnlyTypes = viewModel->onlyTypes;

    viewModel->setFilters( true,
                           expOK,
                           expBrokenLinks,
                           expBrokenImages,
                           expBrokenLinkedImages,
                           (ui->btnFilters->isChecked()?onlyTypes:"")
                );

    // export data
    QFile file(fileName);
    bool opened = file.open(QIODevice::WriteOnly | QIODevice::Text);
    if (opened)
    {
        QTextStream out(&file);

        out << "URL;page;fileType;status;contentType\n";
        int rc = viewModel->rowCount(QModelIndex());
        for (int i = 0; i < rc; i++)
        {
            out << QString("%1;%2;%3;%4;%5\n")
                   .arg( viewModel->data( viewModel->index(i, 1), Qt::DisplayRole).toString() )
                   .arg( viewModel->data( viewModel->index(i, 2), Qt::DisplayRole).toString() )
                   .arg( viewModel->data( viewModel->index(i, 3), Qt::DisplayRole).toString() )
                   .arg( viewModel->data( viewModel->index(i, 4), Qt::DisplayRole).toString() )
                   .arg( viewModel->data( viewModel->index(i, 5), Qt::DisplayRole).toString() );
        }

        file.close();
    }
    else
    {
        QMessageBox::warning(this, tr("Error"), QString(tr("Error opening file for writing (%1)")).arg(file.error()));
    }

    // restore actual filters
    viewModel->setFilters( ui->btnFilters->isChecked(),
                           savedExpOK,
                           savedExpBrokenLinks,
                           savedExpBrokenImages,
                           savedExpBrokenLinkedImages,
                           savedOnlyTypes
                );
}
// ----------------------------------------------------------------------------
void MainWindow::on_cbShowOK_clicked()
{
    viewModel->setFilters(
                ui->btnFilters->isChecked(),
                ui->cbShowOK->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->leShowTypes->text()
    );

    lblCounter->setText(QString(tr("%1 total, %2 filtered"))
                            .arg(viewModel->totalRowCount())
                            .arg(viewModel->rowCount(QModelIndex()))
                        );
}
// ----------------------------------------------------------------------------
void MainWindow::on_cbShowBAD_clicked()
{
    viewModel->setFilters(
                ui->btnFilters->isChecked(),
                ui->cbShowOK->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->leShowTypes->text()
    );

    lblCounter->setText(QString(tr("%1 total, %2 filtered"))
                            .arg(viewModel->totalRowCount())
                            .arg(viewModel->rowCount(QModelIndex()))
                        );
}
// ----------------------------------------------------------------------------
void MainWindow::on_leShowTypes_editingFinished()
{
    viewModel->setFilters(
                ui->btnFilters->isChecked(),
                ui->cbShowOK->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->cbShowBAD->isChecked(),
                ui->leShowTypes->text()
    );

    lblCounter->setText(QString(tr("%1 total, %2 filtered"))
                            .arg(viewModel->totalRowCount())
                            .arg(viewModel->rowCount(QModelIndex()))
                        );
}
// ----------------------------------------------------------------------------
// EOF
