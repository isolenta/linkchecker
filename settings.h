#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
    class Settings;
}

class Settings : public QDialog
{
        Q_OBJECT
        
    public:
        explicit Settings(QWidget *parent = 0);
        ~Settings();
        
    private slots:
        void on_cbUseProxy_clicked(bool checked);
        void displayMenu(const QPoint &pos);
        void on_buttonBox_accepted();

    signals:
        void settingsUpdated();

private:
        Ui::Settings *ui;
        MainWindow *mw;

        void addMappingItem(QString fileType, QString contentType);
};

#endif // SETTINGS_H
