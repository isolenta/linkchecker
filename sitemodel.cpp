// ----------------------------------------------------------------------------
#include "sitemodel.h"
#include "siteitem.h"
#include <QDebug>
#include <QIcon>
#include <QTextCodec>
#include <QtAlgorithms>
// ----------------------------------------------------------------------------
SiteModel::SiteModel(QObject *parent)
{
    setParent(parent);

    useFilters = false;
    expOK = false;
    expBrokenImages = false;
    expBrokenLinkedImages = false;
    expBrokenLinks = false;
    typeMap = NULL;
}
// ----------------------------------------------------------------------------
SiteModel::~SiteModel()
{
}
// ----------------------------------------------------------------------------
void SiteModel::setTypeMap(QHash<QString, QString> *typeMap)
{
    this->typeMap = typeMap;
}
// ----------------------------------------------------------------------------
int SiteModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return viewItems.count();
}
// ----------------------------------------------------------------------------
int SiteModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return 6;
}
// ----------------------------------------------------------------------------
QVariant SiteModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column();

    SiteItem *item = (SiteItem *)&(viewItems[row]);

    switch (col)
    {
        case 0:
            if (role == Qt::DecorationRole)
            {
                if (item->isOk)
                    return QVariant(QIcon(":/icons/green.png"));
                else
                    return QVariant(QIcon(":/icons/red.png"));
            }
            else
                return QVariant();

        case 1:
            if (role == Qt::DisplayRole)
                return QVariant(item->uri);
            else
                return QVariant();

        case 2:
            if (role == Qt::DisplayRole)
                return QVariant(item->page);
            else
                return QVariant();

        case 3:
            if (role == Qt::DisplayRole)
                return QVariant(item->fileType);
            else
                return QVariant();

        case 4:
            if (role == Qt::DisplayRole)
            {
                if (item->isOk)
                    return QVariant("OK");
                else
                {
                    if (item->isImage && item->isLink)
                        return QVariant("A-BAD");
                    else
                        return QVariant("BAD");
                }
            }
            else if (role == Qt::DecorationRole)
            {
                if (!item->isOk && item->isImage && item->isLink)
                    return QVariant(QIcon(":/icons/anchor.png"));
                else
                    return QVariant();
            }
            else if (role == Qt::ToolTipRole)
            {
                if (item->isOk)
                    return QVariant();
                else
                    return QVariant(item->replyCode);
            }
            else
                return QVariant();

        case 5:
            if (role == Qt::DisplayRole)
            {
                QString contentType = "";

                if (item->isOk)
                {
                    contentType = item->contentType;
                    if (typeMap != NULL)
                    {
                        foreach(const QString &key, typeMap->keys())
                            if (item->fileType == key)
                            {
                                contentType = typeMap->value(key);
                                break;
                            }
                    }
                }

                return QVariant(contentType);
            }

        default:
            return QVariant();
    }

    return QVariant();
}
// ----------------------------------------------------------------------------
QVariant SiteModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
    {
        switch (section)
        {
            case 0:
                return QVariant("");
            case 1:
                return QVariant(tr("Link"));
            case 2:
                return QVariant(tr("Page"));
            case 3:
                return QVariant(tr("Filetype"));
            case 4:
                return QVariant(tr("Status"));
            case 5:
                return QVariant(tr("Content-type"));
            default:
                return QVariant();
        }
    }

    return QVariant();
}
// ----------------------------------------------------------------------------
void SiteModel::addSiteItem(const SiteItem &item)
{
    // prevent URI duplicates
    //if (items.contains(item))
      //  return;

    beginInsertRows(QModelIndex(), items.count(), items.count());

    items.append(item);
    applyFilters();

    endInsertRows();
}
// ----------------------------------------------------------------------------
void SiteModel::addSiteItems(QSet<SiteItem> &_items)
{
    if (_items.isEmpty())
        return;

    beginInsertRows(QModelIndex(), items.count(), items.count() + _items.count() - 1);

    QSet<SiteItem>::const_iterator i = _items.constBegin();
    while (i != _items.constEnd())
    {
        items.append(*i);
        ++i;
    }

    applyFilters();

    endInsertRows();
}
// ----------------------------------------------------------------------------
void SiteModel::clear()
{
    emit (beginResetModel());

    items.clear();
    viewItems.clear();

    emit (endResetModel());
}
// ----------------------------------------------------------------------------
bool substringInLine(const QString &substr, const QString &line)
{
    if (substr == "")
        return true;

    QStringList slist = line.split(";");

    // remove dots at mask begin if needed
    for (int i = 0; i < slist.count(); i++)
    {
        QString s = slist[i];
        slist.replace(i, s.remove(QRegExp("^[\\.]+")));
    }

    return slist.contains(substr);
}
// ----------------------------------------------------------------------------
bool SiteModel::isFiltersPassed(const SiteItem &item)
{
    if (!useFilters)
        return true;

    bool filtersPassed;

    filtersPassed = false;

    if (item.isOk)
    {
        if (expOK)
            filtersPassed = true;
    }
    else
    {
        if (item.isLink)
        {
            if (item.isImage)
            {
                if (expBrokenLinkedImages)
                    filtersPassed = true;
            }
            else
            {
                if (expBrokenLinks)
                    filtersPassed = true;
            }
        }
        else
        {
            if (item.isImage)
            {
                if (expBrokenImages)
                    filtersPassed = true;
            }
        }
    } // !ok

    if (onlyTypes != "")
    {
        if (!substringInLine(item.fileType, onlyTypes))
            filtersPassed = false;
    }

    return filtersPassed;
}
// ----------------------------------------------------------------------------
void SiteModel::applyFilters()
{
    viewItems.clear();

    if (!useFilters)
    {
        viewItems = items;
        return;
    }

    QList<SiteItem>::const_iterator i = items.constBegin();
    while (i != items.constEnd())
    {
        if (isFiltersPassed(*i))
            viewItems.append(*i);

        ++i;
    }
}
// ----------------------------------------------------------------------------
void SiteModel::setFilters(bool useFilters,
                  bool expOK,
                  bool expBrokenLinks,
                  bool expBrokenImages,
                  bool expBrokenLinkedImages,
                  QString onlyTypes)
{
    this->useFilters = useFilters;
    this->expOK = expOK;
    this->expBrokenLinks = expBrokenLinks;
    this->expBrokenImages = expBrokenImages;
    this->expBrokenLinkedImages = expBrokenLinkedImages;
    this->onlyTypes = onlyTypes;

    applyFilters();

    // tell view that model invalidated
    emit (beginResetModel());
    emit (endResetModel());
}
// ----------------------------------------------------------------------------
int SiteModel::totalRowCount()
{
    return items.count();
}
// ----------------------------------------------------------------------------
static int _column;
static int _order;

bool siteItemComparator(const SiteItem &left, const SiteItem &right)
{
    bool result = false;
    QString leftStr, rightStr;

    switch (_column)
    {
    case 2:
        result = left.page < right.page;
        break;
    case 3:
        result = left.fileType < right.fileType;
        break;
    case 4:
        if (left.isOk)
            leftStr = "OK";
        else
        {
            if (left.isImage && left.isLink)
                leftStr = "A-BAD";
            else
                leftStr = "BAD";
        }

        if (right.isOk)
            rightStr = "OK";
        else
        {
            if (right.isImage && right.isLink)
                rightStr = "A-BAD";
            else
                rightStr = "BAD";
        }

        result = leftStr < rightStr;

        break;
    case 5:
        result = left.contentType < right.contentType;
        break;
    case 1:
    default:
        result = left.uri < right.uri;
        break;
    }

    if (_order == Qt::AscendingOrder)
        return result;
    else
        return !result;
}

void SiteModel::sort(int column, Qt::SortOrder order)
{
    if ((column == 0) || (column > 5))
        return;

    _column = column;
    _order = order;

    qSort(items.begin(), items.end(), siteItemComparator);

    applyFilters();

    emit (beginResetModel());
    emit (endResetModel());
}
// ----------------------------------------------------------------------------
// EOF
