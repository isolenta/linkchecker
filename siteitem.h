#ifndef SITEITEM_H
#define SITEITEM_H

#include <QObject>
#include <QString>

class SiteItem
{
public:
    //SiteItem(const SiteItem &item);
    //SiteItem();
    QString asString();

    bool operator==(const SiteItem &item) const;

    QString uri;            // full object URL
    QString page;           // page URL hosted this link
    bool isOk;              // good/broken object
    bool isLocal;           // pointer to object, stored on local domain
    QString contentType;    // HTTP content-type
    QString fileType;       // last paro of filename (e.g. ".php", ".gif" ...)
    bool isImage;           // is this image (<img src="...">)
    bool isLink;            // is this link (<a href="...">)
    bool isProcessed;       // is this link already processed
    QString replyCode;      // HTTP reply numeric and string codew
};

uint qHash(const SiteItem &key);

#endif // SITEITEM_H
