#include <QIntValidator>
#include <QSettings>
#include <QComboBox>

#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    this->mw = qobject_cast<MainWindow *>(parent);

    ui->leProxyURL->setText( mw->proxyURL );
    ui->leProxyUser->setText( mw->proxyUser );
    ui->leProxyPass->setText( mw->proxyPass );
    ui->cbUseProxy->setChecked( mw->useProxy );

    ui->leUserAgent->setText( mw->userAgent );
    ui->leNumThreads->setText( QString("%1").arg(mw->numThreads) );
    ui->leNetworkTimeout->setText( QString("%1").arg(mw->networkTimeout) );

    QIntValidator *numThreadsValidator = new QIntValidator(this);
    numThreadsValidator->setRange(2, 25);
    ui->leNumThreads->setValidator(numThreadsValidator);

    QIntValidator *networkTimeoutValidator = new QIntValidator(this);
    ui->leNetworkTimeout->setValidator(networkTimeoutValidator);

    ui->leProxyPass->setEchoMode(QLineEdit::Password);

    ui->leProxyURL->setEnabled( ui->cbUseProxy->isChecked() );
    ui->leProxyUser->setEnabled( ui->cbUseProxy->isChecked() );
    ui->leProxyPass->setEnabled( ui->cbUseProxy->isChecked() );

    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Filetype")));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Content-type")));
    ui->tableWidget->verticalHeader()->hide();
    ui->tableWidget->horizontalHeader()->setResizeMode(QHeaderView::Stretch);

    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->tableWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(displayMenu(QPoint)));

    // type map
    foreach(const QString &key, mw->typeMap.keys())
        addMappingItem(key, mw->typeMap.value(key));
}

Settings::~Settings()
{
    delete ui;
}

void Settings::displayMenu(const QPoint &pos)
{
    QMenu menu(this);

    QModelIndex cell = ui->tableWidget->indexAt(pos);

    if (cell.isValid())
    {
        QAction *delAction = menu.addAction(tr("Delete"));
        QAction *addAction = menu.addAction(tr("Add new"));

        QAction *selectedAction = menu.exec(ui->tableWidget->viewport()->mapToGlobal(pos));

        // remove whole row
        if (selectedAction == delAction)
            ui->tableWidget->removeRow(cell.row());
        // add new row
        else if (selectedAction == addAction)
            addMappingItem("", "text/html");
    }
    else
    {
        QAction *addAction = menu.addAction(tr("Add new"));
        QAction *selectedAction = menu.exec(ui->tableWidget->viewport()->mapToGlobal(pos));

        // add new row
        if (selectedAction == addAction)
            addMappingItem("", "text/html");
    }

}

void Settings::addMappingItem(QString fileType, QString contentType)
{
    ui->tableWidget->setRowCount( ui->tableWidget->rowCount() + 1);

    QTableWidgetItem *itemFileType = new QTableWidgetItem;
    itemFileType->setText(fileType);
    itemFileType->setFlags(itemFileType->flags() ^ Qt::ItemIsSelectable);
    ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 0, itemFileType);

    QComboBox *cb = new QComboBox;

    // some standard mimetypes
    cb->addItem("text/html");
    cb->addItem("text/xml");
    cb->addItem("text/css");
    cb->addItem("image/jpeg");
    cb->addItem("image/png");
    cb->addItem("image/gif");
    cb->addItem("application/octet-stream");
    cb->addItem("application/msword");
    cb->addItem("application/ogg");
    cb->addItem("application/pdf");
    cb->addItem("application/zip");

    cb->setEditable(true);
    int idx;
    if ((idx = cb->findText(contentType)) != -1)
        cb->setCurrentIndex(idx);
    else
    {
        cb->insertItem(0, contentType);
        cb->setCurrentIndex(0);
    }

    QTableWidgetItem *itemStub = new QTableWidgetItem;
    itemStub->setFlags(itemStub->flags() ^ Qt::ItemIsSelectable);
    ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, itemStub);

    ui->tableWidget->setCellWidget(ui->tableWidget->rowCount() - 1, 1, cb);
}

void Settings::on_cbUseProxy_clicked(bool checked)
{
    ui->leProxyURL->setEnabled(checked);
    ui->leProxyUser->setEnabled(checked);
    ui->leProxyPass->setEnabled(checked);
}

void Settings::on_buttonBox_accepted()
{
    mw->useProxy = ui->cbUseProxy->isChecked();
    mw->proxyURL = ui->leProxyURL->text();
    mw->proxyUser = ui->leProxyUser->text();
    mw->proxyPass = ui->leProxyPass->text();
    mw->userAgent = ui->leUserAgent->text();
    mw->numThreads = ui->leNumThreads->text().toInt();
    mw->networkTimeout = ui->leNetworkTimeout->text().toInt();

    mw->typeMap.clear();
    for (int i = 0; i < ui->tableWidget->rowCount(); i++)
    {
        QString type, content;

        type = ui->tableWidget->item(i, 0)->text();
        QModelIndex idx = ui->tableWidget->model()->index(i, 1);
        QComboBox *cb = qobject_cast<QComboBox *>(ui->tableWidget->indexWidget(idx));
        content = cb->currentText();

        mw->typeMap.insert(type, content);
    }

    emit settingsUpdated();
}
