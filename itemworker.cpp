#include <QUrl>
#include <QRegExp>
#include <QFileInfo>
#include <QtXml/QDomDocument>
#include <QThreadPool>
#include <QDebug>
#include <QApplication>
#include "itemworker.h"
#include "lcurl.h"
#include "dispatcher.h"

#include <QMutex>

ItemWorker::ItemWorker(QStack<LcURL *> *_curls, SiteItem _item, QString localDomain)
    :item(_item), curls(_curls)
{    
    this->item.uri = LcURL::encodeURL(this->item.uri);
    lDomain = localDomain;
    requestStop = false;
}

QString ItemWorker::getUrlFileType(const QString &url)
{
    QUrl qurl(url);
    QFileInfo fileInfo(qurl.path());
    return fileInfo.suffix();
}

QString ItemWorker::getUrlDomain(const QString &url)
{
    QUrl qurl(url);
    return qurl.host();
}

bool ItemWorker::isLocalDomain(const QString &link)
{
    QRegExp domainRx("^(.*)\\." + lDomain);

    if (getUrlDomain(link) == lDomain)
        return true;

    if (domainRx.indexIn(getUrlDomain(link), 0) != -1)
        return true;

    return false;
}

void ItemWorker::onStopSignal()
{
    requestStop = true;
}

void ItemWorker::run()
{
    // convert relative URI to absolute
    if (item.page != "")
        item.uri = resolveLink(item.page, item.uri);

    // immediately exit if STOP requested
    if (requestStop)
    {
        emit itemAppend(item);
        return;
    }

    // fill item fields
    QString contentType;
    QString replyCode;

    curlsMutex.lock();
    if (curls->isEmpty())
    {
        //qDebug() << "it: curls empty";
        return;
    }

    curl = curls->pop();
    curlsMutex.unlock();

    item.isOk = curl->checkURL(item.uri, &contentType, &replyCode);

    curlsMutex.lock();
    curls->push(curl);
    curlsMutex.unlock();

    item.isLocal = isLocalDomain(item.uri);
    item.fileType = getUrlFileType(item.uri);
    item.contentType = contentType;
    item.replyCode = replyCode;
    item.isProcessed = true;

    // immediately exit if STOP requested
    if (requestStop)
    {
        emit itemAppend(item);
        return;
    }

    // schedule dispatcher to process new page
    if (item.isLocal &&
            item.isOk &&
            item.isLink &&
            item.contentType == "text/html")
    {
        emit processPage(item.uri);
    }

    // tell dispatcher to append new item to collection and notify that finished
    emit itemAppend(item);
}
